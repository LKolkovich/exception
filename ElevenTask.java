import java.util.Scanner;

public class ElevenTask{
    public static void main(String[] args) throws StringLengthExeption{
        Scanner scan = new Scanner(System.in);
        try {
            String str = scan.nextLine();
            if(str.length() > 10){
                throw new StringLengthExeption("String longer than 10 simbols");
            }
        }
        catch(StringLengthExeption exep){
            System.out.println(exep.getMessage());
        }
    }
}

class StringLengthExeption extends Exception{
    public StringLengthExeption() {
    }

    public StringLengthExeption(String message) {
        super(message);
    }

    public StringLengthExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public StringLengthExeption(Throwable cause) {
        super(cause);
    }

    public StringLengthExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
